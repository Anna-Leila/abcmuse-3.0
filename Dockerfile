FROM python:3-alpine

WORKDIR /code

COPY ./requirements.txt /code/requirements.txt
RUN pip install -r /code/requirements.txt
COPY . /code

RUN python /code/DJANGOabcMuse/manage.py migrate
RUN python /code/DJANGOabcMuse/manage.py collectstatic --no-input

EXPOSE 8000
CMD python /code/DJANGOabcMuse/manage.py runserver 0.0.0.0:8000